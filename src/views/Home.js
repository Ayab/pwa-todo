import { html, render } from 'lit-html';
import { setTodos } from '../idb.js';
import { createTodo } from '../api/todos.js';
import '../component/todo-card.js';

export default class Home {
  constructor(page) {
    this.page = page;
    this.properties = {
      todos: [],
      title: ''
    };

    this.renderView();
  }

  set todos(value) {
    this.properties.todos = value;
  }

  get todos() {
    return this.properties.todos;
  }


  template() {
      return html`
      <section class="h-full">
        <div ?hidden="${!this.todos.length}">
          <header>
            <h1 class="mt-2 px-4 text-xl">My awesome todos : </h1>
          </header>
          <main class="todolist px-4 pb-20">
            <ul>
              ${this.todos.map(todo => html`<todo-card .todo="${todo}"></todo-card>`)}
            </ul>
          </main>
        </div>
        <div class="mt-8" ?hidden="${!!this.todos.length}">
          <p class="mt-4 text-center text-xl">No todos yet, try to create a new one</p>
        </div>
        <footer class="h-16 bg-gray-300 fixed bottom-0 inset-x-0">
          <form @submit="${this.handleForm.bind(this)}" id="addTodo" class="w-full h-full flex justify-between items-center px-4 py-3">
            <label class="flex-1" aria-label="Add todo input">
              <input
                autocomplete="off"
                .value="${this.properties.title}"
                @input="${e => this.properties.title = e.target.value}"
                class="py-3 px-4 rounded-sm w-full h-full"
                type="text"
                placeholder="Enter a new todo ..."
                name="todo">
            </label>
            <button
              aria-label="Add"
              class="ml-4 rounded-lg text-uppercase bg-heraku h-full text-center px-3 uppercase text-white font-bold flex justify-center items-center"
              type="submit">Add<lit-icon class="ml-2" icon="send"></lit-icon></button>
          </form>  
        </footer>
      </section>
    `;
  }

  renderView() {
    const view = this.template();
    render(view, this.page);
  }

  handleForm(e) {
    e.preventDefault();
    console.log(this.properties.title);
    const todo = {
      id: Date.now(),
      title: this.properties.title,
      synced: 'false',
      toUpdate: 'false',
      deleted: 'false'
    }

    this.properties.todos = [...this.properties.todos, todo];

    setTodos(this.properties.todos)
        .then( todo => console.log(" Add to local storage"));

   const create = createTodo(todo)
        .then( create =>  {
          if(create !== false) {
            console.log("Item store item in database");
          } else {
            console.log("Failed to store item in database");
          }
        });

      this.renderView();


  }
}
