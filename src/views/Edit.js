import { render, html } from "lit-html";
import "../component/todo-card.js";

export default class Edit {
    constructor (page) {
        this.page = page;
        this.properties = {
            todo: "",
        };

        this.renderView();
    }

    set todo (value) {
        this.properties.todo = value;
    }

    get todo () {
        return this.properties.todo;
    }

    template () {
        return html`
      <section class="h-full container px-5 mt-5 mx-auto">
          <div class="max-w text-center bg-gray-300 rounded overflow-hidden shadow-lg">
              <div class="px-6 py-4">
                <div class="font-bold text-xl mb-2">${this.todo.title}</div> 
                <span class="align-text-bottom">Edit Todo </span>
              </div>
            </div>
        </main>
      </section>
    `;
    }

    renderView () {
        const view = this.template();
        render(view, this.page);
    }
}
