/**
 *
 */
export async function fetchTodos() {
  const config = window.config;
  return fetch('http://localhost:3000/todos', {
    method: 'GET',
    headers: {
      'Content-type': 'application/json'
    }
  })
      .then(result => result.json())
      .catch(error => {
        console.error(error);
        return false;
      })
}

export function fetchTodo(id) {
  const config = window.config;

  return fetch(`http://localhost:3000/todos/${id}`, {
    method: 'GET'
  })
      .then(result => result.json())
      .catch(error => {
        console.error(error);
        return false;
      })
}

export function createTodo(data) {
  const config = window.config;
  return fetch('http://localhost:3000/todos', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(data)
  })
      .then(result => result.json())
      .catch(error => {
        console.error(error);
        return false;
      });
}

export function updateTodo(data) {
  const config = window.config;

  return fetch(`http://localhost:3000/todos/${data.id}`, {
    method: 'PUT',
    headers: {
      'Content-type': 'application/json'
    },
    body: JSON.stringify(data)
  })
      .then(result => result.json())
      .catch(error => {
        console.error(error);
        return false;
      })
}

export async function deleteTodo(id) {
  const config = window.config;
  return fetch(`http://localhost:3000/todos/${id}`, {
    method: 'DELETE',
    headers: {
      'Content-Type': 'application/json',
    }
  })
      .then((result) => {result.json(); window.location.reload(true)})
      .catch(error => {
        console.error(error);
        return false;
      });
}