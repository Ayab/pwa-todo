import page from "page";
import { fetchTodos, fetchTodo, createTodo, deleteTodo, updateTodo } from "./api/todos.js";
import { setTodos, setTodo, unsetTodo, getTodos, getTodo } from "./idb.js";


const app = document.querySelector('#app');
fetch('./config.json')
  .then(result => result.json())
  .then(async (config) => {
    // Set the config
    window.config = config;

    const link = document.createElement('link');
    link.setAttribute('rel', 'stylesheet');
    link.setAttribute('href', './assets/styles/tailwind.css');
    document.head.appendChild(link);

      page('/', async (ctx) => {
          const module = await import('./views/Home.js');
          const Home = module.default;

          let todos = [];
          if (navigator.onLine) {
              const data = await fetchTodos();
              if (data !== false) {
                  todos = await setTodos(data);
              }
              todos = await getTodos();
          } else {
              todos = await getTodos();
          }


          const ctn = app.querySelector('[page="home"]');
          const HomeView = new Home(ctn, todos);

          ctn.setAttribute('active', true);
      });

      document.addEventListener("delete-todo", async ({ detail }) => {
          if (!document.offline && navigator.onLine === true) {
              const result = await deleteTodo(detail.id);
              if (result !== false) {
                  const todo = await unsetTodo(detail.id);
                  return document.dispatchEvent(new CustomEvent("render-view", { detail: todo }));
              }
              detail.deleted = "true";
          }
      });

      page('/todos/:id', async (context) => {
          const module = await import('./views/Edit');
          const Edit = module.default;

          const ctn = app.querySelector("[page=\"Edit\"]");
          const editView = new Edit(ctn);

          let todo = {};
          if (!document.offline && navigator.onLine) {
              todo = await fetchTodo(context.params.id);
          } else {
              todo = await getTodo(context.params.id) || [];
          }

          editView.todo = todo;
          editView.renderView();
          displayPage("Edit")
      });

      document.addEventListener('update-todo', async ({detail: todo}) => {
          await deleteTodo(todo.id)
          await setTodo(todo);
          if (!document.offline && navigator.onLine === true) {
              const result = await updateTodo(todo);
              if (result !== false) {
                  page(`/todos/${todo.id}`)
              }
              page(`/todos/${todo.id}`)
          }
      });



// Start router
      page();

  });

function displayPage(page) {
    const skeleton = document.querySelector('#app .skeleton');
    skeleton.removeAttribute('hidden');
    const pages = app.querySelectorAll('[page]');
    pages.forEach(page => page.removeAttribute('active'));
    skeleton.setAttribute('hidden', 'true');
    const p = app.querySelector(`[page="${page}"]`);
    p.setAttribute('active', true);
}

